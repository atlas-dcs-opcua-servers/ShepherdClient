

#include <iostream>
#include <Shepherd.h>
#include <uaclient/uasession.h>
#include <stdexcept>
#include <UaoClientForShepherdArrayTools.h>
#include <UaoClientForShepherdUaoExceptions.h>

namespace UaoClientForShepherd
{


Shepherd::
Shepherd
(
    UaClientSdk::UaSession* session,
    UaNodeId objId
) :
    m_session(session),
    m_objId (objId)
{

}




void Shepherd::registerSheep (
    const std::vector<UaString>&  in_sheepNames,
    const UaString&  in_endpointUrl,
    const UaString&  in_serverUri
)
{

    ServiceSettings serviceSettings;
    CallIn callRequest;
    CallOut co;

    callRequest.objectId = m_objId;
    callRequest.methodId = UaNodeId( UaString(m_objId.identifierString()) + UaString(".registerSheep"), 2 );


    UaVariant v;


    callRequest.inputArguments.create( 3 );
    ArrayTools::convertVectorToUaVariant(in_sheepNames, v);



    v.copyTo( &callRequest.inputArguments[ 0 ] );

    v.setString( in_endpointUrl );



    v.copyTo( &callRequest.inputArguments[ 1 ] );

    v.setString( in_serverUri );



    v.copyTo( &callRequest.inputArguments[ 2 ] );



    UaStatus status =
        m_session->call(
            serviceSettings,
            callRequest,
            co
        );
    if (status.isBad())
        throw Exceptions::BadStatusCode("In OPC-UA call", status.statusCode());




}


void Shepherd::resolveSheep (
    const std::vector<UaString>&  in_sheepNames,
    std::vector<UaString>& out_resolutionStatus,
    std::vector<UaString>& out_endpointUrls,
    std::vector<UaString>& out_serverUris
)
{

    ServiceSettings serviceSettings;
    CallIn callRequest;
    CallOut co;

    callRequest.objectId = m_objId;
    callRequest.methodId = UaNodeId( UaString(m_objId.identifierString()) + UaString(".resolveSheep"), 2 );


    UaVariant v;


    callRequest.inputArguments.create( 1 );
    ArrayTools::convertVectorToUaVariant(in_sheepNames, v);



    v.copyTo( &callRequest.inputArguments[ 0 ] );



    UaStatus status =
        m_session->call(
            serviceSettings,
            callRequest,
            co
        );
    if (status.isBad())
        throw Exceptions::BadStatusCode("In OPC-UA call", status.statusCode());


    v = co.outputArguments[0];

    ArrayTools::convertUaVariantToVector( v, out_resolutionStatus );

    v = co.outputArguments[1];

    ArrayTools::convertUaVariantToVector( v, out_endpointUrls );

    v = co.outputArguments[2];

    ArrayTools::convertUaVariantToVector( v, out_serverUris );



}



}