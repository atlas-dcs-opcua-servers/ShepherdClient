#include <ShepherdResolver.hpp>

#include <Shepherd.h>

using namespace UaoClientForShepherd;

namespace ShepherdClient
{

std::string resolutionStatusToString (ResolutionStatus resolutionStatus)
{
    switch (resolutionStatus)
    {
        case ResolutionStatus::Good: return "Good";
        case ResolutionStatus::Uncertain: return "Uncertain";
        case ResolutionStatus::Bad: return "Bad";
        default: throw std::runtime_error("not-implemented");
    }
}

std::string resolutionSourceToString (ResolutionSource resolutionSource)
{
    switch (resolutionSource)
    {
        case ResolutionSource::OnlineValue: return "OnlineValue";
        case ResolutionSource::ServerCache: return "ServerCache";
        case ResolutionSource::ClientCache: return "ClientCache";
        case ResolutionSource::NoData: return "NoData(!!)";
        default: throw std::runtime_error("not-implemented");
    }
}

ShepherdResolver::ShepherdResolver (const std::string& shepherdEndpoint):
    m_shepherdServerEndpoint (shepherdEndpoint)
{

}

SheepResolutionReply ShepherdResolver::resolveSheep (const std::string& sheepName)
{
    SheepResolutionReply reply;

    // TODO: so far - no caching.
    // TODO: RAII for mem-mngmt.
    UaSession session;

    UaClientSdk::SessionSecurityInfo security;
    UaClientSdk::SessionConnectInfo sessionConnectInfo;
    UaStatus status = session.connect( // TODO: there we will probably move to sth fancier.
        m_shepherdServerEndpoint.c_str(),
        sessionConnectInfo,     // General settings for connection
        security,               // Security settings
        nullptr );              // Callback interface


    Shepherd shepherd (&session, UaNodeId("theShepherd", 2));
    std::vector<UaString> justOneSheepName {sheepName.c_str()};
    std::vector<UaString> resolutionStatus;
    std::vector<UaString> endpointUrls;
    std::vector<UaString> serverUris;
    shepherd.resolveSheep(
        justOneSheepName,
        resolutionStatus,
        endpointUrls,
        serverUris);
    // TODO: error handling from there

    if (resolutionStatus.size() != 1 || endpointUrls.size() != 1 || serverUris.size() != 1)
        throw std::runtime_error("resolve reply fault: vectors are of different sizes");
    
    if (resolutionStatus[0] == "GO")
    {
        reply.resolutionStatus = Good;
        reply.resolutionSource = OnlineValue;
        reply.endpointUrl = endpointUrls[0].toUtf8();
        reply.serverUri = serverUris[0].toUtf8();
    }
    else if (resolutionStatus[0] == "USC")
    {
        reply.resolutionStatus = Uncertain;
        reply.resolutionSource = ServerCache;
        reply.endpointUrl = endpointUrls[0].toUtf8();
        reply.serverUri = serverUris[0].toUtf8();
    }
    else if (resolutionStatus[0] == "BND")
    {
        reply.resolutionStatus = Bad;
        reply.resolutionSource = NoData;
    }
    else
        throw std::runtime_error("resolve reply fault: Unable to interpret status [" + resolutionStatus[0].toUtf8()+"]. Major version mismatch?");

    ServiceSettings defaultServiceSettings;
    session.disconnect(defaultServiceSettings, OpcUa_False);

    return reply;
}


}