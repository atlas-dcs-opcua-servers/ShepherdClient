#include <iostream>
#include <iomanip>

#include <boost/program_options.hpp>

#include <ShepherdResolver.hpp>

namespace po = boost::program_options;

using namespace ShepherdClient;

int main(int argc, char* argv[])
{
    // program options to query
    // where is the shepherd server,
    // and what logical name we want
    // to resolve.

    std::string shepherdEndpoint;
    std::vector<std::string> sheepToResolve;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h",              "produce help message")
        ("shepherd_enspoint,s", po::value<std::string>(&shepherdEndpoint)->default_value("opc.tcp://localhost:48888"), "shepherd endpoint, starting with opc.tcp://")
        ("sheep",               po::value<std::vector<std::string>>(&sheepToResolve)->required(), "one or multiple sheep to resolve (can skip --sheep if last args)");
    
    po::positional_options_description positional;
    positional.add("sheep", -1);


    po::variables_map vm;
    try
    {
        po::store(po::command_line_parser(argc, argv).options(desc).positional(positional).run(), vm);
        po::notify(vm);        
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
 
    if (vm.count("help"))
    {
        std::cout << desc << std::endl;
        return 1;
    }

    ShepherdClient::ShepherdResolver shepherdResolver (shepherdEndpoint);
    std::map<std::string, SheepResolutionReply> results;

    for (std::string sheepName : vm["sheep"].as<std::vector<std::string>>())
    {
        std::cout << "Resolving " << sheepName << std::endl;
        SheepResolutionReply reply = shepherdResolver.resolveSheep(sheepName);       
        results[sheepName] = reply;
    }

    std::cout << " --- Summary --- " << std::endl;
    std::cout << "Queried name    ||  Status   |    Source    |               Endpoint URL               | Server URI" << std::endl;
    std::cout << "----------------++-----------+--------------+------------------------------------------+-----------" << std::endl;
    for (std::string sheepName : vm["sheep"].as<std::vector<std::string>>())
    {
        SheepResolutionReply& reply = results[sheepName];
        std::cout << std::setw(15) << std::left <<  sheepName;
        std::cout << " || ";
        std::cout << std::setw(9) << std::left << resolutionStatusToString(reply.resolutionStatus);
        std::cout << " | ";
        std::cout << std::setw(12) << std::left << resolutionSourceToString(reply.resolutionSource);
        std::cout << " | ";
        std::cout << std::setw(40) << reply.endpointUrl;
        std::cout << " | ";
        std::cout << std::setw(10) << reply.serverUri;
        std::cout << std::endl;
    }

    

}