#pragma once

#include <string>

namespace ShepherdClient
{

enum ResolutionStatus
{
    Good = 0,
    Uncertain = 1, // data was GOOD AND CERTAIN in the past but at this moment we can only used past value.
    Bad = 2
};

std::string resolutionStatusToString (ResolutionStatus resolutionStatus);

enum ResolutionSource
{
    OnlineValue = 0,
    ServerCache = 1,
    ClientCache = 2,
    NoData = 3
};

std::string resolutionSourceToString (ResolutionSource resolutionSource);

struct SheepResolutionReply
{
    ResolutionStatus resolutionStatus;
    ResolutionSource resolutionSource;
    std::string      endpointUrl;
    std::string      serverUri;
    SheepResolutionReply() : resolutionStatus(Bad), resolutionSource(NoData) {}
};

class ShepherdResolver
{
// write some documentation here

public:
    ShepherdResolver (const std::string& shepherdEndpoint);

    //! re-entrant (internally locked)
    SheepResolutionReply resolveSheep (const std::string& sheepName);

private:
    const std::string m_shepherdServerEndpoint;
};

}