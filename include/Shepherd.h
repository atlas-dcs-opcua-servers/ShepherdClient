
#ifndef __UAO__UaoClientForShepherd__Shepherd__
#define __UAO__UaoClientForShepherd__Shepherd__

#include <iostream>
#include <uaclient/uaclientsdk.h>

namespace UaoClientForShepherd
{

using namespace UaClientSdk;



class Shepherd
{

public:

    Shepherd(
        UaClientSdk::UaSession* session,
        UaNodeId objId
    );

// getters, setters for all variables

    void registerSheep(
        const std::vector<UaString>&  in_sheepNames,
        const UaString&  in_endpointUrl,
        const UaString&  in_serverUri
    );

    void resolveSheep(
        const std::vector<UaString>&  in_sheepNames,
        std::vector<UaString>& out_resolutionStatus,
        std::vector<UaString>& out_endpointUrls,
        std::vector<UaString>& out_serverUris
    );


private:

    UaClientSdk::UaSession* m_session;
    UaNodeId                m_objId;

};



}

#endif // __UAO__UaoClientForShepherd__Shepherd__